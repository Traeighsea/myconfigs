set encoding=utf-8

"----Plugin Stuffs----
" Init Plugins
call plug#begin('~/.vim/plugged')

" File tree bar
Plug 'scrooloose/nerdtree'

" Better tagline interface
Plug 'vim-airline/vim-airline'

" Ui Icons
Plug 'ryanoasis/vim-devicons'

" Linter
"Plug 'vim-syntastic/syntastic'

" Autocompletion
"Plug 'ycm-core/YouCompleteMe'
"Plug 'xavierd/clang_complete'

" LSP Support
"Plug 'prabirshrestha/async.vim'
"Plug 'prabirshrestha/vim-lsp'
"Plug 'ajh17/vimcompletesme'

" Code outline:
Plug 'preservim/tagbar'

" Source/header jumping:
"Plug 'derekwyatt/vim-fswitch'

" Prototype definitions?:
"Plug 'derekwyatt/vim-protodef'

" Git integration
"Plug 'airblade/vim-gitgutter'
"Plug 'tpope/vim-fugitive'

"  Colorthemes
Plug 'vim-airline/vim-airline-themes'
Plug 'sickill/vim-monokai'
Plug 'dracula/vim'
Plug 'Rigellute/shades-of-purple.vim'

"  Additional Colorthemes
"Plug 'ayu-theme/ayu-vim'
"Plug 'joshdick/onedark.vim'
"Plug 'tomasr/molokai'
"Plug 'rakr/vim-one'
"Plug 'nanotech/jellybeans.vim'
"Plug 'w0ng/vim-hybrid'
"Plug 'junegunn/seoul256.vim'
"Plug 'jacoborus/tender.vim'
"Plug 'kaicataldo/material.vim'
"Plug 'sainnhe/sonokai'
"Plug 'rktjmp/lush.nvim'

call plug#end()

" Additional commands
autocmd VimEnter * NERDTree | wincmd p
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" Plugin Settings
let g:tagbar_autofocus = 1
let g:tagbar_autoshowtag = 1
let g:tagbar_position = 'botright vertical'

"let g:clang_library_path='/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/libclang.dylib'

" Linter Settings
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:shades_of_purple_airline = 1
let g:airline_theme='shades_of_purple'

:command Tree NERDTreeToggle

nmap <F2> :NERDTreeToggle
nmap <F8> :TagbarToggle<CR>

"if executable('clangd')
"    augroup lsp_clangd
"        autocmd!
"        autocmd User lsp_setup call lsp#register_server({
"                    \ 'name': 'clangd',
"                    \ 'cmd': {server_info->['clangd']},
"                    \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
"                    \ })
"        autocmd FileType c setlocal omnifunc=lsp#complete
"        autocmd FileType cpp setlocal omnifunc=lsp#complete
"        autocmd FileType objc setlocal omnifunc=lsp#complete
"        autocmd FileType objcpp setlocal omnifunc=lsp#complete
"    augroup end
"endif

"----End Plugin Stuffs----

"----Base Vim Settings----

" Colors Scheme
syntax on
colorscheme shades_of_purple

" Formatting and Layout
set linebreak
set showbreak=---
set textwidth=0
set showmatch
set ruler
set number
set softtabstop=3
set tabstop=3
set shiftwidth=3

" Editor Settings
set hlsearch
set smartcase
set spell spelllang=en_us
set autoindent
set expandtab
set smartindent
set smarttab
set mouse=a
set undolevels=100
set backspace=indent,eol,start
set belloff=all

" IDE type variables
set wildmenu 
"set omnifunc=syntaxcomplete#Complete
set omnifunc=ClangComplete
set completefunc=ClangComplete
set makeprg=cmake
set tags=./tags,$HOME/tags
set tags+=~/Documents/Projects/*/tags

" Additional functions
function! SwitchSourceHeader()
  "update!
  if (expand ("%:e") == "cpp")
    vsplit %:t:r.hpp
  else
    vsplit %:t:r.cpp
  endif
endfunction

" Macros
" Command to open term with vsplit by default
:command MyTerm "rightb vert term"

" Keybinds 
nnoremap <Down> <C-w>j
nnoremap <Up> <C-w>k
nnoremap <Left> <C-w>h
nnoremap <Right> <C-w>l
nnoremap ,s :call SwitchSourceHeader()<CR>



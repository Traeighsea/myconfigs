export TERM="xterm-256color"

alias shellify="chmod u+x"

alias vimrc="vim ~/.vimrc"
alias svimrc="source ~/.vimrc"
alias bashrc="vim ~/.bashrc"
alias sbashrc="source ~/.bashrc"
alias zshrc="vim ~/.zshrc"
alias szshrc="source ~/.zshrc"

alias buildtags="~/Scripts/buildtags.sh"

export PATH="/usr/local/opt/llvm/bin:$PATH"
# export LDFLAGS="-L/usr/local/opt/llvm/lib"
# export CPPFLAGS="-I/usr/local/opt/llvm/include"
